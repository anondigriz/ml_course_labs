# Лабораторная работа 1

## Создание виртуального окружения ([Windows](https://tyapk.ru/blog/post/python-virtual-environment-windows))

Создаётся через модуль virtualenv.

```shell
pip install virtualenv
```

Используется команда `python −m virtualenv` и название директории, в которой будет создано виртуальное окружение.

```shell
mkdir project
cd project
python -m virtualenv env
```

Использование:

Для активации окружение надо запустить скрипт `activate.bat` внутри директории с виртуальным окружением. Допустим, окружение создано в директории `e:\home\python\project\`.

```shell
e:\home\python\project\env\Scripts\activate.bat
pip install jupyterhub 
pip install jupyterlab
e:\home\python\project\env\Scripts\deactivate.bat
```

## Запуск Jupyter

```shell
jupyter lab
```

## Сборка pdf-отчета

```shell
D:
D:\ml_cource\mlenv\Scripts\activate.bat
cd D:\gitlab\ml_course_labs\
jupyter nbconvert --to pdf --template=../common/template.tplx notebook.ipynb
```